import { Component, OnInit } from '@angular/core';
import { Login } from 'src/app/components/login/login';
import { DataSharingService } from 'src/app/services/data-sharing-service.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  isUserLoggedIn: boolean = false;
  user: Login = new Login("","");

  constructor(private dataSharingService: DataSharingService) {
    this.dataSharingService.isUserLoggedIn.subscribe(value => {
      this.isUserLoggedIn = value;
    });
    this.dataSharingService.user.subscribe(value => {
      this.user = value;
    });
  }
  

  ngOnInit(): void {
  }


  logout() {
    this.dataSharingService.isUserLoggedIn.next(false);
    this.dataSharingService.user.next(new Login("",""));
  }

}

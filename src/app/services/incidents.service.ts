import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http' 
import { Login } from '../components/login/login';
import { Incident } from '../components/incident-form/incident';
import { Update } from '../components/update-form/update';
@Injectable({
  providedIn: 'root'
})
export class IncidentsService {

  baseUrl = "http://localhost:8080"
  authorizationData = 'Basic '+ btoa("admin:password");
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': this.authorizationData,
      'Access-Control-Allow-Origin': '*'
    })
  };


  constructor(private httpClient: HttpClient) { }

  getIncidents(){
    return this.httpClient.get(`${this.baseUrl}/incident/`,this.httpOptions);
  }

  getIncidentById(idIncident:number){
    return this.httpClient.get(`${this.baseUrl}/incident/${idIncident}`,this.httpOptions);
  }

  getUpdateById(idUpdate:number){
    return this.httpClient.get(`${this.baseUrl}/update/${idUpdate}`,this.httpOptions);
  }

  getUpdatesByIncident(idIncident:string){
    return this.httpClient.get(`${this.baseUrl}/update/incident/${idIncident}`,this.httpOptions);
  }
  
  access(login:Login){
    return this.httpClient.post(`${this.baseUrl}/user/login`,login,this.httpOptions);
  }

  saveIncident(incident:Incident){
    return this.httpClient.post(`${this.baseUrl}/incident/`,incident,this.httpOptions);
  }

  saveUpdate(update:Update){
    return this.httpClient.post(`${this.baseUrl}/update/`,update,this.httpOptions);
  }

  deleteIncident(idIncident:number){
    return this.httpClient.delete(`${this.baseUrl}/incident/${idIncident}`,this.httpOptions);
  }

  deleteUpdate(idUpdate:number){
    return this.httpClient.delete(`${this.baseUrl}/update/${idUpdate}`,this.httpOptions);
  }

}

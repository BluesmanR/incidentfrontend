import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Login } from '../components/login/login';

@Injectable({
  providedIn: 'root'
})
export class DataSharingService {

  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  public user: BehaviorSubject<Login> = new BehaviorSubject<Login>(new Login("",""));
}

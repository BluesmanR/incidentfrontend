import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataSharingService } from 'src/app/services/data-sharing-service.service';
import { IncidentsService } from 'src/app/services/incidents.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  
  updates: any[] = [];
  idIncident: string = "";
  isUserLoggedIn: boolean = false;

  constructor(private route: ActivatedRoute, private incidentsService: IncidentsService,  private dataSharingService: DataSharingService ) {
    
    this.dataSharingService.isUserLoggedIn.subscribe(value => {
      this.isUserLoggedIn = value;
    });
    route.params.subscribe(data =>{
      this.idIncident = data['id'];
      console.log(this.idIncident);
      this.getUpdates();
    })
   }

  ngOnInit(): void {
  }
  getUpdates(){
    this.incidentsService.getUpdatesByIncident(this.idIncident)
        .subscribe((data:any) => {
          console.log(data);
          this.updates = data;
        })
  }

  deleteIncident(idIncident:number){
    this.incidentsService.deleteUpdate(idIncident)
      .subscribe(()=> this.getUpdates())
  }

}

export class Incident {
    constructor(
        public title: string,
        public description: string,
        public stats: string,
        public start: Date,
        public finish: Date,
        public status: string
    ){}
}
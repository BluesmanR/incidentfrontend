import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IncidentsService } from 'src/app/services/incidents.service';
import { Incident } from './incident';

@Component({
  selector: 'app-incident-form',
  templateUrl: './incident-form.component.html',
  styleUrls: ['./incident-form.component.css']
})
export class IncidentFormComponent implements OnInit {

  incident = new Incident("","","",new Date(),new Date(),"");
  idIncident: number = 0;
  oldStatus:string = "";

  constructor(private incidentService: IncidentsService, private router: Router, private activatedRoute: ActivatedRoute) { 
    activatedRoute.params.subscribe(data =>{
      this.idIncident = data['idIncident'];
    });
    
    if(this.idIncident != 0){
      incidentService.getIncidentById(this.idIncident)
        .subscribe((data:any) => {
          this.incident = data;
          this.oldStatus = this.incident.status;
          console.log(this.oldStatus);
        
        });
    }
   }

  ngOnInit(): void {
  }
  submitIncident(){
    console.log(this.incident);    
    if(this.oldStatus == "Open" && this.oldStatus != this.incident.status){
      this.incident.finish = new Date();
    }
    this.incidentService.saveIncident(this.incident).subscribe((data:any) =>{
      console.log(data);
      this.router.navigate(['/home']);
    })
  }
}

export class Update {
    constructor(
        public idUpdate: number,
        public type: string,
        public updateContent: string,
        public updateDt: Date,
        public idIncident: number
    ){}
}
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IncidentsService } from 'src/app/services/incidents.service';
import { Update } from './update';

@Component({
  selector: 'app-update-form',
  templateUrl: './update-form.component.html',
  styleUrls: ['./update-form.component.css']
})
export class UpdateFormComponent implements OnInit {

  update = new Update(0,"","",new Date(),0);
  idIncident: number = 0;
  idUpdate: number = 0;

  constructor(private activatedRoute: ActivatedRoute, private router: Router, private incidentService: IncidentsService) {
    activatedRoute.params.subscribe(data =>{
      this.idIncident = data['idIncident'];
      this.idUpdate = data['idUpdate'];
    });
    if(this.idUpdate != 0){
      this.incidentService.getUpdateById(this.idUpdate)
        .subscribe((data:any) => {
          console.log(data);
          this.update = data;
        } );
    }
  }

  ngOnInit(): void {
   
  }
  submitUpdate(){
    console.log(this.update);
    this.update.idIncident = this.idIncident;
    this.incidentService.saveUpdate(this.update).subscribe((data:any) =>{
      console.log(data);
      this.router.navigate([ `/details/${this.idIncident}`]);
    })
  
  }
}

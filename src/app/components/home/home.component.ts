import { Component, OnInit } from '@angular/core';
import { DataSharingService } from 'src/app/services/data-sharing-service.service';
import { IncidentsService } from 'src/app/services/incidents.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  incidents: any[] = [];
  isUserLoggedIn: boolean = false;

  constructor(public incidentsService: IncidentsService, private dataSharingService: DataSharingService) {
    //this.incidentsService.getIncidents();
    this.getIncidents();
    this.dataSharingService.isUserLoggedIn.subscribe(value => {
      this.isUserLoggedIn = value;
    });
   
   }

  ngOnInit(): void {
  }
  getIncidents(){
    this.incidentsService.getIncidents().subscribe((data:any)=>{
      console.log(data);
      this.incidents = data;
    });
  }

  deleteIncident(idIncident: number){
    console.log(idIncident);
    this.incidentsService.deleteIncident(idIncident).subscribe((value) =>{
      this.getIncidents();
    })
  }

}

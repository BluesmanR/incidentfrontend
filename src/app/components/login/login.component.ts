import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataSharingService } from 'src/app/services/data-sharing-service.service';
import { IncidentsService } from 'src/app/services/incidents.service';
import { Login } from './login';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginModel = new Login("","");
  //isLoged: boolean = false;
  constructor(private incidentService: IncidentsService, private router:Router, private dataSharingService: DataSharingService) { }

  ngOnInit(): void {
  }
  sendForm(){
    this.incidentService.access(this.loginModel)
      .subscribe((data:any)=> {
        console.log(data);
        if(data.login){
          //sessionStorage.setItem("session",JSON.stringify(this.loginModel));
          
          this.dataSharingService.user.next(this.loginModel);
          this.dataSharingService.isUserLoggedIn.next(true);
          this.router.navigate(['/home']);
         
        }else{
          alert("Wrong username or password");
        }
      });
    console.log(this.loginModel);
  }

}

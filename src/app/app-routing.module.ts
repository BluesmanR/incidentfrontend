import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailsComponent } from './components/details/details.component';
import { HomeComponent } from './components/home/home.component';
import { IncidentFormComponent } from './components/incident-form/incident-form.component';
import { LoginComponent } from './components/login/login.component';
import { UpdateFormComponent } from './components/update-form/update-form.component';

const routes: Routes = [
  {path: 'home', component: HomeComponent},
  {path: 'details/:id', component: DetailsComponent},
  {path: 'login', component: LoginComponent},
  {path: 'incident/:idIncident', component: IncidentFormComponent},
  {path: 'update/:idIncident/:idUpdate', component: UpdateFormComponent},
  {path: '', pathMatch: 'full', redirectTo: 'home'},
  {path: '**', pathMatch: 'full', redirectTo: 'home'},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
